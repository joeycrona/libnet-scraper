"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Nightmare = require("nightmare");
require('nightmare-download-manager')(Nightmare);
var fs = require("fs");
var start = Date.now();
var results = [];
function run(offset) {
    return __awaiter(this, void 0, void 0, function () {
        var nightmare, path, ctx, p, res;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    nightmare = Nightmare({ show: false });
                    nightmare.on('download', function (state, downloadItem) {
                        if (state == 'started') {
                            nightmare.emit('download', path, downloadItem);
                        }
                    });
                    console.log('start');
                    path = '';
                    ctx = nightmare
                        .downloadManager()
                        .goto('https://www.libnet.fi/login.html')
                        .select('[name="_language"', 'en')
                        .wait(100)
                        .type('#j_username', 'Demoaccount')
                        .wait(100)
                        .type('#j_password', 'Welkom2017')
                        .wait(100)
                        .click('[name="login"]')
                        .wait('[title="Publications"]');
                    p = offset + 1;
                    _a.label = 1;
                case 1:
                    if (!(p < (offset + 10000))) return [3 /*break*/, 6];
                    return [4 /*yield*/, ctx
                            .goto('https://www.libnet.fi/julkaisut/julkaisutiedot.html?publicationId=' + p)
                            .evaluate(function (p) {
                            var desc = document.querySelector('.odd td');
                            var img = document.querySelector('.detailview-layout img');
                            var out = {
                                name: document.querySelector('#contentbody h2').innerText.replace('Publication information: ', ''),
                                desc: desc ? desc.innerText : null,
                                img: img ? img.src : null
                            };
                            for (var i = 1; i < 22; i++) {
                                var key = document.querySelector('.publishingDates tr:nth-child(' + i + ') th');
                                if (key) {
                                    out[key.innerText] =
                                        document.querySelector('.publishingDates tr:nth-child(' + i + ') td').innerText;
                                }
                            }
                            if (out.img && out.img != 'https://www.libnet.fi/images/NoCoverImageAvailable.gif') {
                                var a = document.createElement('a');
                                a.href = out.img;
                                a.download = './imgs/libnet-sub-' + p + '.img';
                                document.body.appendChild(a);
                            }
                            return out;
                        }, p)];
                case 2:
                    res = _a.sent();
                    results.push(res);
                    if (!(res.img && res.img != 'https://www.libnet.fi/images/NoCoverImageAvailable.gif')) return [3 /*break*/, 4];
                    path = './imgs/libnet-sub-' + p + '.img';
                    return [4 /*yield*/, ctx.click('a[href="' + res.img + '"]')
                            .waitDownloadsComplete()];
                case 3:
                    _a.sent();
                    _a.label = 4;
                case 4:
                    if (results.length % 10 == 0) {
                        console.log(results.length, Date.now() - start);
                    }
                    _a.label = 5;
                case 5:
                    p++;
                    return [3 /*break*/, 1];
                case 6:
                    ctx.end()
                        .then(function (res) {
                        fs.writeFileSync('./subs' + p + '.json', JSON.stringify(results));
                    })
                        .catch(function (error) {
                        console.error('Search failed:', error);
                    });
                    return [2 /*return*/];
            }
        });
    });
}
run(20000).then(console.log).catch(console.error);
