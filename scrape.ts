import * as Nightmare from 'nightmare';
require('nightmare-download-manager')(Nightmare);
import * as fs from 'fs';

var start = Date.now();



declare var document: any;

let results = [];


async function run(offset) {
    const nightmare = Nightmare({ show: false });
    nightmare.on('download', function (state, downloadItem) {
        if (state == 'started') {
            nightmare.emit('download', path, downloadItem);
        }
    });
    console.log('start');
    var path = '';
    let ctx = nightmare
        .downloadManager()
        .goto('https://www.libnet.fi/login.html')
        .select('[name="_language"', 'en')
        .wait(100)
        .type('#j_username', 'Demoaccount')
        .wait(100)
        .type('#j_password', 'Welkom2017')
        .wait(100)
        .click('[name="login"]')
        .wait('[title="Publications"]');

        
    for (var p = offset+1; p < (offset+10000); p++) {
        let res = await ctx
            .goto('https://www.libnet.fi/julkaisut/julkaisutiedot.html?publicationId=' + p)
            .evaluate((p) => {
                let desc = document.querySelector('.odd td');
                let img = document.querySelector('.detailview-layout img');
                let out = {
                    name: document.querySelector('#contentbody h2').innerText.replace('Publication information: ', ''),
                    desc: desc ? desc.innerText : null,
                    img: img ? img.src : null
                };

                for (let i = 1; i < 22; i++) {
                    let key = document.querySelector('.publishingDates tr:nth-child(' + i + ') th');
                    if (key) {
                        out[key.innerText] =
                            document.querySelector('.publishingDates tr:nth-child(' + i + ') td').innerText;
                    }
                }

                if (out.img && out.img != 'https://www.libnet.fi/images/NoCoverImageAvailable.gif') {
                    var a = document.createElement('a');
                    a.href = out.img;
                    a.download = './imgs/libnet-sub-' + p + '.img';
                    document.body.appendChild(a);

                }
                return out;
            }, p);
        results.push(res)
        if (res.img && res.img != 'https://www.libnet.fi/images/NoCoverImageAvailable.gif') {
            path = './imgs/libnet-sub-' + p + '.img';
            await ctx.click('a[href="' + res.img + '"]')
                .waitDownloadsComplete()

        }
        if(results.length%10==0){
            console.log(results.length,Date.now()-start);
        }
    }
    ctx.end()
        .then(res => {
            fs.writeFileSync('./subs'+p+'.json',JSON.stringify(results));
        })
        .catch((error: any) => {
            console.error('Search failed:', error);
        });

}

    run(20000).then(console.log).catch(console.error);